package com.bbva.qwai.batch.mapper;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.bbva.qwai.dto.domain.CustomerDTO;

public class CustomerFieldSetMapper implements FieldSetMapper<CustomerDTO>{

    @Override
    public CustomerDTO mapFieldSet(FieldSet fieldSet) throws BindException {
   	 CustomerDTO customerDTO = new CustomerDTO();
   	 customerDTO.setNationality(fieldSet.readString(0));
   	 customerDTO.setEntityCode(fieldSet.readString(1));
   	 customerDTO.setCustomerId(fieldSet.readString(2));
   	 
   	 customerDTO.setFirstName(fieldSet.readString(3));
   	 customerDTO.setLastName(fieldSet.readString(4) + " " + fieldSet.readString(5));
   	 customerDTO.setIdentityDocumentType(fieldSet.readString(6));
   	 customerDTO.setIdentityDocumentNumber(fieldSet.readString(7));
   	 customerDTO.setBirthDate(fieldSet.readDate(8, "dd/MM/yy"));
   	 customerDTO.setGenderId(fieldSet.readString(9));
   	 customerDTO.setPersonalTitle(fieldSet.readString(10));
   	 customerDTO.setMaritalStatus(fieldSet.readString(11));
   	 return customerDTO;
    }

}


