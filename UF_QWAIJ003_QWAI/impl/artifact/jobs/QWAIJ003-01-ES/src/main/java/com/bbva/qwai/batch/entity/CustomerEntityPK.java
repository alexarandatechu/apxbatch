package com.bbva.qwai.batch.entity;


import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CustomerEntityPK {

    @Column(name = "COD_PAISOALF")
    private String nationality;
    
    @Column(name = "COD_ENTALFA")
    private String codEntity;
    
    @Column(name = "COD_PERSCTPN")
    private String customerId;

    public String getNationality() {
   	 return nationality;
    }

    public void setNationality(String nationality) {
   	 this.nationality = nationality;
    }

    public String getCustomerId() {
   	 return customerId;
    }

    public void setCustomerId(String customerId) {
   	 this.customerId = customerId;
    }

    public String getCodEntity() {
   	 return codEntity;
    }

    public void setCodEntity(String codEntity) {
   	 this.codEntity = codEntity;
    }

    @Override
    public int hashCode() {
   	 final int prime = 31;
   	 int result = 1;
   	 result = prime * result + ((codEntity == null) ? 0 : codEntity.hashCode());
   	 result = prime * result + ((customerId == null) ? 0 : customerId.hashCode());
   	 result = prime * result + ((nationality == null) ? 0 : nationality.hashCode());
   	 return result;
    }

    @Override
    public boolean equals(Object obj) {
   	 if (this == obj)
   		 return true;
   	 if (obj == null)
   		 return false;
   	 if (getClass() != obj.getClass())
   		 return false;
   	 CustomerEntityPK other = (CustomerEntityPK) obj;
   	 if (codEntity == null) {
   		 if (other.codEntity != null)
   			 return false;
   	 } else if (!codEntity.equals(other.codEntity))
   		 return false;
   	 if (customerId == null) {
   		 if (other.customerId != null)
   			 return false;
   	 } else if (!customerId.equals(other.customerId))
   		 return false;
   	 if (nationality == null) {
   		 if (other.nationality != null)
   			 return false;
   	 } else if (!nationality.equals(other.nationality))
   		 return false;
   	 return true;
    }

    @Override
    public String toString() {
   	 return "CustomerEntityPK [nationality=" + nationality + ", codEntity=" + codEntity + ", customerId="
   			 + customerId + "]";
    }

    

    
    
}

