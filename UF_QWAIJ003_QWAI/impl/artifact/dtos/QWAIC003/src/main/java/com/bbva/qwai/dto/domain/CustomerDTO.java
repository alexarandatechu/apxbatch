package com.bbva.qwai.dto.domain;


import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.bbva.apx.dto.AbstractDTO;
/**
 * Clase Ejemplo para desarrollo de un DTO fuera del modelo canonico
 * @author beeva
 *
 */
public class CustomerDTO extends AbstractDTO {
    private static final long serialVersionUID = 2931699728946643245L;
   
    private String customerId;
    private String entityCode;
    private String firstName;
    private String lastName;
    private String nationality;
    private String personalTitle;
    private String genderId;
    private String identityDocumentType;
    private String identityDocumentNumber;
    private Date birthDate;
    private String maritalStatus;
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getEntityCode() {
		return entityCode;
	}
	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getPersonalTitle() {
		return personalTitle;
	}
	public void setPersonalTitle(String personalTitle) {
		this.personalTitle = personalTitle;
	}
	public String getGenderId() {
		return genderId;
	}
	public void setGenderId(String genderId) {
		this.genderId = genderId;
	}
	public String getIdentityDocumentType() {
		return identityDocumentType;
	}
	public void setIdentityDocumentType(String identityDocumentType) {
		this.identityDocumentType = identityDocumentType;
	}
	public String getIdentityDocumentNumber() {
		return identityDocumentNumber;
	}
	public void setIdentityDocumentNumber(String identityDocumentNumber) {
		this.identityDocumentNumber = identityDocumentNumber;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result + ((customerId == null) ? 0 : customerId.hashCode());
		result = prime * result + ((entityCode == null) ? 0 : entityCode.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((genderId == null) ? 0 : genderId.hashCode());
		result = prime * result + ((identityDocumentNumber == null) ? 0 : identityDocumentNumber.hashCode());
		result = prime * result + ((identityDocumentType == null) ? 0 : identityDocumentType.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((maritalStatus == null) ? 0 : maritalStatus.hashCode());
		result = prime * result + ((nationality == null) ? 0 : nationality.hashCode());
		result = prime * result + ((personalTitle == null) ? 0 : personalTitle.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerDTO other = (CustomerDTO) obj;
		if (birthDate == null) {
			if (other.birthDate != null)
				return false;
		} else if (!birthDate.equals(other.birthDate))
			return false;
		if (customerId == null) {
			if (other.customerId != null)
				return false;
		} else if (!customerId.equals(other.customerId))
			return false;
		if (entityCode == null) {
			if (other.entityCode != null)
				return false;
		} else if (!entityCode.equals(other.entityCode))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (genderId == null) {
			if (other.genderId != null)
				return false;
		} else if (!genderId.equals(other.genderId))
			return false;
		if (identityDocumentNumber == null) {
			if (other.identityDocumentNumber != null)
				return false;
		} else if (!identityDocumentNumber.equals(other.identityDocumentNumber))
			return false;
		if (identityDocumentType == null) {
			if (other.identityDocumentType != null)
				return false;
		} else if (!identityDocumentType.equals(other.identityDocumentType))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (maritalStatus == null) {
			if (other.maritalStatus != null)
				return false;
		} else if (!maritalStatus.equals(other.maritalStatus))
			return false;
		if (nationality == null) {
			if (other.nationality != null)
				return false;
		} else if (!nationality.equals(other.nationality))
			return false;
		if (personalTitle == null) {
			if (other.personalTitle != null)
				return false;
		} else if (!personalTitle.equals(other.personalTitle))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CustomerDTO [customerId=" + customerId + ", entityCode=" + entityCode + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", nationality=" + nationality + ", personalTitle=" + personalTitle
				+ ", genderId=" + genderId + ", identityDocumentType=" + identityDocumentType
				+ ", identityDocumentNumber=" + identityDocumentNumber + ", birthDate=" + birthDate + ", maritalStatus="
				+ maritalStatus + "]";
	}

    
    
}
