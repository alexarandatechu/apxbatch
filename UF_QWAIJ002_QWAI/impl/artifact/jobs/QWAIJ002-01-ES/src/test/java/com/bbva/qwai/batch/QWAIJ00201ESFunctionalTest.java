package com.bbva.qwai.batch;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test for batch process QWAIJ002-01-ES
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/batch/beans/QWAIJ002-01-ES-beans.xml","classpath:/META-INF/spring/batch/jobs/jobs-QWAIJ002-01-ES-context.xml","classpath:/META-INF/spring/jobs-QWAIJ002-01-ES-runner-context.xml"})
public class QWAIJ00201ESFunctionalTest{

	@Autowired
	private JobLauncherTestUtils jobLauncherTestUtils;


	@Test
	public void testLaunchJob() throws Exception {
		//TODO implements job launch test
		//Without parameters (use this implementation if job not need parameters)
		//final JobExecution jobExecution = jobLauncherTestUtils.launchJob();
		
		//With parameters (use this implementation if job needs parameters comment first implementation) 
		/*********************** Parameters Definition ***********************/
		//First parameter
		final JobParameter jobParameter = new JobParameter("test.txt");
		//Add parameters to job
		final HashMap<String, JobParameter> parameters = new HashMap<String, JobParameter>();
		parameters.put("itemsFile", jobParameter);
		parameters.put("outputFile", new JobParameter("/home/alumno/Escritorio/output.txt"));
		final JobParameters jobParameters = new JobParameters(parameters);
		final JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
		
		//TODO implements job launch test Assert's
		Assert.assertTrue(jobExecution.getExitStatus().equals(ExitStatus.COMPLETED));
	}
}
