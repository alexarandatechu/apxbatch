package com.bbva.qwai.batch.processor;

import org.springframework.batch.item.ItemProcessor;

public class HelloWorldItemProcessor implements ItemProcessor<String, String>{

	@Override
	public String process(String arg0) throws Exception {
		// arg0 = "Hello|World"
		// lo transformamos a "Hello World"
		String[] split = arg0.split(",");
		return split[0] + " " + split[1];
	}
	
	

}
